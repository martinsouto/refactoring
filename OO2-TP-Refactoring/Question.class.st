"
Please comment me using the following template inspired by Class Responsibility Collaborator (CRC) design:
 
For the Class part:  State a one line summary. For example, ""I represent a paragraph of text"".

For the Responsibility part: Three sentences about my main responsibilities - what I do, what I know.

For the Collaborators Part: State my main collaborators and one line about how I interact with them. 

Public API and Key Messages

- message one   
- message two 
- (for bonus points) how to create instances.

   One simple example is simply gorgeous.
 
Internal Representation and Key Implementation Points.

    Instance Variables
	answers:		<Object>
	description:		<Object>
	timestamp:		<Object>
	title:		<Object>
	topics:		<Object>
	user:		<Object>
	votes:		<Object>


    Implementation Points
"
Class {
	#name : #Question,
	#superclass : #Post,
	#instVars : [
		'title',
		'answers',
		'topics'
	],
	#category : #'OO2-TP-Refactoring-Model'
}

{ #category : #'instance creation' }
Question class >> newWithTitle: title description: aDescription user: aUser [
	^ (self newWithUser: aUser description: aDescription)
		initializeWithTitle: title;
		yourself
]

{ #category : #'instance creation' }
Question class >> newWithTitle: title description: aDescription user: aUser topic: aTopic [
	^ (self newWithUser: aUser description: aDescription)
		initializeWithTitle: title topic: aTopic;
		yourself
]

{ #category : #adding }
Question >> addTopic: aTopic [
	topics add: aTopic.
	aTopic addQuestion: self.

]

{ #category : #'initalize-release' }
Question >> initialize [
	super initialize.
	answers := OrderedCollection new.
	topics := OrderedCollection new.
]

{ #category : #'initalize-release' }
Question >> initializeWithTitle: aTitle [
	title := aTitle.
]

{ #category : #'initalize-release' }
Question >> initializeWithTitle: aTitle topic: aTopic [
	title := aTitle.
	self addTopic: aTopic.
]

{ #category : #accessing }
Question >> title [
	^title 
]

{ #category : #accessing }
Question >> topics [
	^topics 
]
