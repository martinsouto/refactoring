Object subclass: #Conexion
	instanceVariableNames: ''
	classVariableNames: ''
	package: 'OO2-TP-Pattern'!
!Conexion commentStamp: 'Fedebaretto 5/26/2021 17:48' prior: 0!
Please comment me using the following template inspired by Class Responsibility Collaborator (CRC) design:
 
For the Class part:  State a one line summary. For example, "I represent a paragraph of text".

For the Responsibility part: Three sentences about my main responsibilities - what I do, what I know.

For the Collaborators Part: State my main collaborators and one line about how I interact with them. 

Public API and Key Messages

- message one   
- message two 
- (for bonus points) how to create instances.

   One simple example is simply gorgeous.
 
Internal Representation and Key Implementation Points.


    Implementation Points!


!Conexion methodsFor: 'accessing' stamp: 'Fedebaretto 5/26/2021 18:23'!
enviar: mensaje
	^mensaje! !


Conexion subclass: #ConexionSegura
	instanceVariableNames: 'encriptador'
	classVariableNames: ''
	package: 'OO2-TP-Pattern'!
!ConexionSegura commentStamp: 'Fedebaretto 5/26/2021 17:48' prior: 0!
Please comment me using the following template inspired by Class Responsibility Collaborator (CRC) design: 

For the Class part:  State a one line summary. For example, "I represent a paragraph of text".

For the Responsibility part: Three sentences about my main responsibilities - what I do, what I know.

For the Collaborators Part: State my main collaborators and one line about how I interact with them. 

Public API and Key Messages

- message one   
- message two 
- (for bonus points) how to create instances.

   One simple example is simply gorgeous.
 
Internal Representation and Key Implementation Points.

    Instance Variables
	encriptador:		<Object>


    Implementation Points!


!ConexionSegura methodsFor: 'accessing' stamp: 'Fedebaretto 5/26/2021 18:00'!
cambiarEncriptador: unEncriptador
	encriptador := unEncriptador ! !

!ConexionSegura methodsFor: 'accessing' stamp: 'Fedebaretto 5/26/2021 17:46'!
enviar: mensaje
	| mensajeEncriptado |
	
	mensajeEncriptado := self encriptador encriptar: mensaje.
	super enviar: mensajeEncriptado .! !

!ConexionSegura methodsFor: 'accessing' stamp: 'Fedebaretto 5/26/2021 17:47'!
encriptador
	^encriptador ! !


!ConexionSegura methodsFor: 'initialization' stamp: 'Fedebaretto 5/26/2021 18:28'!
initialize 
	encriptador := EncriptadorRSA new.! !


Object subclass: #Encriptador
	instanceVariableNames: ''
	classVariableNames: ''
	package: 'OO2-TP-Pattern'!
!Encriptador commentStamp: 'Fedebaretto 5/26/2021 17:48' prior: 0!
Please comment me using the following template inspired by Class Responsibility Collaborator (CRC) design:
 
For the Class part:  State a one line summary. For example, "I represent a paragraph of text".

For the Responsibility part: Three sentences about my main responsibilities - what I do, what I know.

For the Collaborators Part: State my main collaborators and one line about how I interact with them. 

Public API and Key Messages

- message one   
- message two 
- (for bonus points) how to create instances.

   One simple example is simply gorgeous.
 
Internal Representation and Key Implementation Points.


    Implementation Points!


!Encriptador methodsFor: 'accessing' stamp: 'Fedebaretto 5/26/2021 17:48'!
encriptar: unString
	^self subclassResponsibility ! !


Encriptador subclass: #EncriptadorBlowfish
	instanceVariableNames: ''
	classVariableNames: ''
	package: 'OO2-TP-Pattern'!
!EncriptadorBlowfish commentStamp: 'Fedebaretto 5/26/2021 17:48' prior: 0!
Please comment me using the following template inspired by Class Responsibility Collaborator (CRC) design:
 
For the Class part:  State a one line summary. For example, "I represent a paragraph of text".

For the Responsibility part: Three sentences about my main responsibilities - what I do, what I know.

For the Collaborators Part: State my main collaborators and one line about how I interact with them. 

Public API and Key Messages

- message one   
- message two 
- (for bonus points) how to create instances.

   One simple example is simply gorgeous.
 
Internal Representation and Key Implementation Points.


    Implementation Points!


!EncriptadorBlowfish methodsFor: 'accessing' stamp: 'Fedebaretto 5/26/2021 17:48'!
encriptar: unString
	^unString! !


Encriptador subclass: #EncriptadorRC4
	instanceVariableNames: ''
	classVariableNames: ''
	package: 'OO2-TP-Pattern'!
!EncriptadorRC4 commentStamp: 'Fedebaretto 5/26/2021 17:48' prior: 0!
Please comment me using the following template inspired by Class Responsibility Collaborator (CRC) design:
 
For the Class part:  State a one line summary. For example, "I represent a paragraph of text".

For the Responsibility part: Three sentences about my main responsibilities - what I do, what I know.

For the Collaborators Part: State my main collaborators and one line about how I interact with them. 

Public API and Key Messages

- message one   
- message two 
- (for bonus points) how to create instances.

   One simple example is simply gorgeous.
 
Internal Representation and Key Implementation Points.


    Implementation Points!


!EncriptadorRC4 methodsFor: 'accessing' stamp: 'Fedebaretto 5/26/2021 17:49'!
encriptar: unString
	^unString! !


Encriptador subclass: #EncriptadorRSA
	instanceVariableNames: ''
	classVariableNames: ''
	package: 'OO2-TP-Pattern'!
!EncriptadorRSA commentStamp: 'Fedebaretto 5/26/2021 17:48' prior: 0!
Please comment me using the following template inspired by Class Responsibility Collaborator (CRC) design:
 
For the Class part:  State a one line summary. For example, "I represent a paragraph of text".

For the Responsibility part: Three sentences about my main responsibilities - what I do, what I know.

For the Collaborators Part: State my main collaborators and one line about how I interact with them. 

Public API and Key Messages

- message one   
- message two 
- (for bonus points) how to create instances.

   One simple example is simply gorgeous.
 
Internal Representation and Key Implementation Points.


    Implementation Points!


!EncriptadorRSA methodsFor: 'accessing' stamp: 'Fedebaretto 5/26/2021 17:49'!
encriptar: unString
	^unString! !


Object subclass: #Mensajero
	instanceVariableNames: 'conexion'
	classVariableNames: ''
	package: 'OO2-TP-Pattern'!
!Mensajero commentStamp: 'Fedebaretto 5/26/2021 17:48' prior: 0!
Please comment me using the following template inspired by Class Responsibility Collaborator (CRC) design:
 
For the Class part:  State a one line summary. For example, "I represent a paragraph of text".

For the Responsibility part: Three sentences about my main responsibilities - what I do, what I know.

For the Collaborators Part: State my main collaborators and one line about how I interact with them. 

Public API and Key Messages

- message one   
- message two 
- (for bonus points) how to create instances.

   One simple example is simply gorgeous.
 
Internal Representation and Key Implementation Points.

    Instance Variables
	conexion:		<Object>


    Implementation Points!


!Mensajero methodsFor: 'accessing' stamp: 'Fedebaretto 5/26/2021 17:50'!
enviar: mensaje
	conexion enviar: mensaje
! !