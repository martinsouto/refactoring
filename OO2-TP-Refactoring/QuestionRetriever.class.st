"
Please comment me using the following template inspired by Class Responsibility Collaborator (CRC) design:
 
For the Class part:  State a one line summary. For example, ""I represent a paragraph of text"".

For the Responsibility part: Three sentences about my main responsibilities - what I do, what I know.

For the Collaborators Part: State my main collaborators and one line about how I interact with them. 

Public API and Key Messages

- message one   
- message two 
- (for bonus points) how to create instances.

   One simple example is simply gorgeous.
 
Internal Representation and Key Implementation Points.

    Instance Variables
	cuoora:		<Object>
	option:		<Object>


    Implementation Points
"
Class {
	#name : #QuestionRetriever,
	#superclass : #Object,
	#instVars : [
		'cuoora'
	],
	#category : #'OO2-TP-Refactoring-Model'
}

{ #category : #'instance creation' }
QuestionRetriever class >> new: cuoora [
	^ self new initializeWithCuoora: cuoora
]

{ #category : #initialization }
QuestionRetriever >> initializeWithCuoora: aCuoora [
	cuoora := aCuoora
]

{ #category : #private }
QuestionRetriever >> orderAndSelect: aCollection forUser: aUser [
	^ (self rejectOwnQuestionsCol: ((self orderCollection: aCollection)  last: (100 min: aCollection 	size)) user: aUser)
]

{ #category : #private }
QuestionRetriever >> orderCollection: aCollection [
	^(aCollection
		asSortedCollection: [ :a :b | a positiveVotes size > b positiveVotes size ])
]

{ #category : #private }
QuestionRetriever >> rejectOwnQuestionsCol: aCol user: aUser [
	^ (aCol reject: [ :q | q user = aUser ])
]

{ #category : #retrieving }
QuestionRetriever >> retrieveQuestions: aUser [
	self subclassResponsibility
]
