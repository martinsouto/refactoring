"
Please comment me using the following template inspired by Class Responsibility Collaborator (CRC) design: 

For the Class part:  State a one line summary. For example, ""I represent a paragraph of text"".

For the Responsibility part: Three sentences about my main responsibilities - what I do, what I know.

For the Collaborators Part: State my main collaborators and one line about how I interact with them. 

Public API and Key Messages

- message one   
- message two 
- (for bonus points) how to create instances.

   One simple example is simply gorgeous.
 
Internal Representation and Key Implementation Points.

    Instance Variables
	description:		<Object>
	timestamp:		<Object>
	user:		<Object>
	votes:		<Object>


    Implementation Points
"
Class {
	#name : #Post,
	#superclass : #Object,
	#instVars : [
		'timestamp',
		'user',
		'votes',
		'description'
	],
	#category : #'OO2-TP-Refactoring-Model'
}

{ #category : #'instance creation' }
Post class >> newWithUser: anUser description: anAnswer [
	^ self new
		initializeWithUser: anUser description: anAnswer;
		yourself
]

{ #category : #adding }
Post >> addVote: aVote [
	votes add: aVote
]

{ #category : #accessing }
Post >> description [
	^ description
]

{ #category : #initialize }
Post >> initialize [
	votes := OrderedCollection new.
	timestamp := DateAndTime now.
]

{ #category : #initialize }
Post >> initializeWithUser: anUser description: anAnswer [
	user := anUser.
	description := anAnswer
]

{ #category : #'as yet unclassified' }
Post >> negativeVotes [
	^(votes reject: [ :vote | vote isLike ])
]

{ #category : #'as yet unclassified' }
Post >> positiveVotes [
	^votes select: [ :vote | vote isLike ]
]

{ #category : #accessing }
Post >> timestamp [
	^ timestamp
]

{ #category : #accessing }
Post >> user [
	^ user
]

{ #category : #accessing }
Post >> votes [
	^ votes
]
